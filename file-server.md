---
title: 'Creating a Cross-Platform File Server Using ASP.NET Core'
date: '2020-01-01'
---

# Introduction

Hey! Today we're going to create a simple file server. This file server will contain endpoints that can create, read, update, and delete files from a specified directory. We'll write the server using ASP.NET Core, and place it into a Docker container, enabling us to build once and deploy anywhere.

# Prerequisites

We'll need a few things before we get started.

* [Visual Studio Code](https://code.visualstudio.com)
* [.NET Core](https://dotnet.microsoft.com/download)
* [Docker](https://www.docker.com)

Download them from the links above and install them.

# ASP.NET Core

Like I was saying earlier, we're going to use ASP.NET Core to develop the file server. ASP.NET Core is the perfect choice for us since it has support for building web APIs out of the box. Also, since ASP.NET Core is built on top of .NET Core, our file server will be cross-platform.

## Getting Started

First things first, open up the folder where you want the file server to live in Visual Studio Code. Then, open up the integrated terminal.

In the integrated terminal, execute:
```bash
dotnet new webapi -o FileServer
```

This command creates a template of a web API project for us. We'll fill out the template with what we need for the file server.

# Docker